package dbus.sink;

import java.util.ArrayList;
import java.util.List;

public class HRow {
    private byte[] rowkey;
    private List<HCell> cells = new ArrayList<>();

    public HRow() {
    }

    public HRow(byte[] rowkey) {
        this.rowkey = rowkey;
    }

    public byte[] getRowkey() {
        return rowkey;
    }

    public void setRowkey(byte[] rowkey) {
        this.rowkey = rowkey;
    }

    public List<HCell> getCells() {
        return cells;
    }

    public void setCells(List<HCell> cells) {
        this.cells = cells;
    }

    public void addCell(String family, String qualifier, byte[] value) {
        HCell hCell = new HCell(family, qualifier, value);
        cells.add(hCell);
    }


    public class HCell{
        private String family;
        private String qualifier;
        private byte[] value;

        public HCell() {
        }

        public HCell(String family, String qualifier, byte[] value) {
            this.family = family;
            this.qualifier = qualifier;
            this.value = value;
        }

        public String getFamily() {
            return family;
        }

        public void setFamily(String family) {
            this.family = family;
        }

        public String getQualifier() {
            return qualifier;
        }

        public void setQualifier(String qualifier) {
            this.qualifier = qualifier;
        }

        public byte[] getValue() {
            return value;
        }

        public void setValue(byte[] value) {
            this.value = value;
        }
    }
}
