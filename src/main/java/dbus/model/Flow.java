package dbus.model;

import enums.FlowStatusEnum;
import enums.HBaseStorageModeEnum;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Flow  implements Serializable {

    private Integer flowId;
    /**
     * HBase中的存储类型, 默认统一存为String,
     */
    private int mode= HBaseStorageModeEnum.STRING.getCode();
    /**
     * 数据库名/schema名
     */
    private String databaseName;
    /**
     * mysql表名
     */
    private String tableName;
    /**
     * hbase表名
     */
    private String hbaseTable;
    /**
     * 默认统一Column Family名称
     */
    private String family;
    /**
     * 字段名转大写, 默认为true
     */
    private boolean uppercaseQualifier=true;
    /**
     * 批量提交的大小, ETL中用到
     */
    private int commitBatch;
    /**
     *  组成rowkey的字段名，必须用逗号分隔
     */
    private String rowKey;
    /**
     * 状态
     */
    private int status= FlowStatusEnum.FLOWSTATUS_INIT.getCode();
}
