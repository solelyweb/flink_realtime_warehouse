package batch.retry;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
/*
mvn install:install-file -Dfile=D:\Downloads\000代码+PPT\simulatedata-generator-0.0.1.jar -DgroupId=com.cloudwise.toushibao -DartifactId=simulatedata-generator -Dversion=0.0.1 -Dpackaging=jar
 */
import java.util.concurrent.TimeUnit;

public class RetryDemo {
    public static void main(String[] args) throws Exception {
        //获取一个运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
            /**
             *  10s中错误重试三次
             */
//        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(
//                3,
//                Time.of(10, TimeUnit.SECONDS)
//        ));
        /**
         *1个小时中, 10s内允许失败2次
         */
        env.setRestartStrategy(RestartStrategies.failureRateRestart(
                2,
                Time.of(1, TimeUnit.HOURS),
                Time.of(10, TimeUnit.SECONDS)
        ));

        //读取数据
        DataSet<String> data = env.fromElements("1", "2", "", "4", "5");

        data.map(new MapFunction<String, Integer>() {
            @Override
            public Integer map(String s) throws Exception {
                return Integer.parseInt(s);
            }
        }).print();
    }
}
