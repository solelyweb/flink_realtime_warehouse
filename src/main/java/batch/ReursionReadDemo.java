package batch;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.configuration.Configuration;


public class ReursionReadDemo {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        Configuration conf = new Configuration();

        //设置递归参数
        conf.setBoolean("recursive.file.enumeration",true);
        DataSource<String> ds = env.readTextFile("D:\\testData").withParameters(conf);
        ds.print();
    }
}
