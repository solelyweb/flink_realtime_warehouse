package batch;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple3;

public class ReadCsvFile {
    public static void main(String[] args) throws Exception {
        //获取一个执行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        //读取输入数据
        DataSource<Tuple3<Integer, Integer, String>> csvDS = env.readCsvFile("D:\\Downloads\\000代码+PPT\\数据源\\user.csv")
                .includeFields("11100")     //对于csv文件中的字段是否读取1代表读取,0代表不读取
                .ignoreFirstLine()          //是否忽视第一行
                .ignoreInvalidLines()       //对于不合法的行是否忽视
                .ignoreComments("##")       //忽视带有##号的行
                .lineDelimiter("\n")        //行分隔符
                .fieldDelimiter(",")        //列分隔符
                .types(Integer.class,Integer.class,String.class);       //转换出的字段类型

        csvDS.print();
    }
}
